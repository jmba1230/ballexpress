import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import MainPage from './MainPage';
import * as serviceWorker from './serviceWorker';


// import bootstrap
import 'bootstrap/dist/css/bootstrap.css';

// import antd
import 'antd/dist/antd.css';

// import toast styles
import 'react-toastify/dist/ReactToastify.css';
import {toast} from 'react-toastify';

toast.configure({
  autoClose: 3000,
	draggable: false,
	hideProgressBar: true,
	newestOnTop: true,
	closeOnClick: true
})

ReactDOM.render(
  // <React.StrictMode>
    <MainPage/>,
  //</React.StrictMode>
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
