import React from 'react';

const Footer=props=>{
    return(
        <div className='footer d-flex justify-content-center align-items-center text-white'
        style={{
            position: 'fixed',
            bottom: '0',
            width: '100%',
            backgroundColor: '#282828',
            height: '80px'
        }}>
            <p>&copy; ballExpress. All Rights Reserved 2020.</p>
        </div>
    )
}

export default Footer;