import React, { useEffect,useState } from 'react';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';
import {Link} from 'react-router-dom';

const adminItems = [
    {
        name: 'Home',
        link: '/homeadmin'
    },
    {
        name: 'Locations',
        link: '/locations'
    },
    {
        name: 'Surfaces',
        link: '/surfaces'
    },
    {
        name: 'Courts',
        link: '/courts'
    },
    {
        name: 'Logout',
        link: '/login'
    }   
];

const userItems =[
    {
        name: 'Home',
        link: '/home'
    },
    {
        name: 'Bookings',
        link: '/schedule'
    },
    {
        name: 'Logout',
        link: '/login'
    },
]

const NavBar=()=>{

    const [navItems, setNavItems]=useState([]);
    const [collapsed, setCollapsed] = useState(true);
    const toggleNavbar = () => setCollapsed(!collapsed);

    useEffect(()=>{

        if(sessionStorage.isAdmin === 'true'){
            setNavItems(adminItems);
        }else{
            setNavItems(userItems);
        }
    })


    return(
        <Navbar color="faded" light>
        <NavbarBrand href="/" className="mr-auto"><em style={{fontWeight:900, fontSize:'2.5rem'}}>ball<span style={{color:'orange'}}>Express</span></em></NavbarBrand>
        <NavbarToggler onClick={toggleNavbar} className="mr-2" />
        <Collapse isOpen={collapsed} navbar>
          <Nav navbar>
            <NavItem className="mx-auto">
            {navItems.map((nav,index)=>(
            <Link 
            key={index}
            to={nav.link}
            style={{
                fontSize: '20px',
                fontWeight: 600,
                margin: 5
            }}
            className="navbar-link"
            >{nav.name}</Link>
            ))}
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>

                 /* <h5><span className='badge-warning'
            style={{
                position: "absolute",
                top: '0.5rem',
                left: '2rem',
            }}>
                <span style={{color:'black', fontWeight:'bolder', backgroundColor:'orange'}}>logged in as: {sessionStorage.userName}</span></span></h5>
        
        <NavbarBrand><img 
        src={logo} 
        width="250px"
        height="100px"
        top="20px"
        className="d-inline-block align-top"
        alt=" "
        /></NavbarBrand>
            <Nav className='mx-auto'>
            <NavItem>
            
          
            {navItems.map((nav,index)=>(
            <Link 
            key={index}
            to={nav.link}
            style={{
                color:'orange',
                fontSize: '20px',
                margin: 5
            }}
            >{nav.name}</Link>
            ))}
           
            </NavItem>
            </Nav> */
      )
}
export default NavBar;