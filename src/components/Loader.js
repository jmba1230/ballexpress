import React from 'react';
import ReactLoading from 'react-loading';

const Loader = ()=>{
    return (
        <div
            className='d-flex justify-content-center align-items-center vh-100'
        >
            <ReactLoading
                color = {'orange'}
                type={"balls"}
                height={'30%'}
                width = {'30%'} 
            />
        </div>
    )
}

export default Loader;