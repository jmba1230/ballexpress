import React from 'react';
import { Button } from 'reactstrap';

const CourtRow = ({ 
    court,
    deleteCourt,
  setShowForm,
  setIsEditing,
  setCourtToEdit
 }) => {
    return (
        <tr>
            <td>{court.courtName}</td>
            <td>{court.courtImg}</td>
            <td>{court.surface?.surfaceType}</td>
            <td>{court.location?.locationName}</td>
            <td>Php {court.price}</td>
            <td>
            <Button
                     style={{
                        color:'#282828',
                      backgroundColor: 'orange'
                  }}
                    onClick={()=>{
                        setShowForm(true);
                        setIsEditing(true);
                        setCourtToEdit(court);
                    }}>Edit</Button>{' '}
                <Button
                     style={{
                        color:'#282828',
                      backgroundColor: 'orange'
                  }}
                    onClick={()=>deleteCourt(court._id)}
                >Delete</Button>
            </td>
        </tr>
    )
}

export default CourtRow;
