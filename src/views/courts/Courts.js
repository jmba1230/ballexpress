  import React, { useState, useEffect } from 'react';
import NavBar from '../../components/NavBar';
import {Container, Row, Card, CardHeader,Button,CardBody, Table} from 'reactstrap';
import CourtRow from './CourtRow';
import CourtForm from './CourtForm';
import Footer from '../../components/Footer';
import Loader from '../../components/Loader';

const Courts=props=>{

  // this state will show/hide the add form
  const [showForm, setShowForm]=useState(false);

   // This state tells t he app if we are editing an item or not
  const [isEditing, setIsEditing]=useState(false);
  const [courtToEdit, setCourtToEdit]=useState({});


  const [courts, setCourts]=useState([]);
  const [surfaces, setSurfaces]=useState([]);
  const [locations, setLocations]=useState([]);

    

  const [isLoading, setIsLoading]=useState(false);

    // retrieve location data using useEffect
    useEffect(()=>{

        // retrieval of all court
        fetch('https://shielded-shelf-13155.herokuapp.com/admin/courts')
        .then(res=>res.json())
        .then(responseFromDB=>{
            // console.log(responseFromDB);
            setCourts(responseFromDB);
            setIsLoading(false);
        })

             // fetch all surfaces
             fetch('https://shielded-shelf-13155.herokuapp.com/admin/surfaces')
             .then(res=>res.json())
             .then(res=>{
                 setSurfaces(res);
             });
     
             // Fetch all locations
             fetch('https://shielded-shelf-13155.herokuapp.com/admin/locations')
             .then(res=>res.json())
             .then(res=>{
     
                 setLocations(res);
             });
    }, []);

  const saveCourt=(
    courtName,
     courtImg,
      surface,
       location,
        price
           )=>{

            setIsLoading(true);

            if(isEditing){
              let editingcourtName = courtName;
              let editingcourtImg = courtImg;
              let editingsurface = surface;
              let editinglocation = location;
              let editingprice = price;

             if (courtName==='')editingcourtName=courtToEdit.courtName;
             if (courtImg==='')editingcourtImg=courtToEdit.courtImg;
             if (surface==='')editingsurface=courtToEdit.surface;
             if (location==='')editinglocation=courtToEdit.location;
             if (price==='')editingprice=courtToEdit.price;
           
           const apiOptions={
             method: "PATCH",
             headers: {'Content-Type': 'application/json'},
             body: JSON.stringify({
               id: courtToEdit._id,
               courtName: editingcourtName,
               courtImg: editingcourtImg,
               surface: editingsurface,
               location: editinglocation,
               price: editingprice
             })
           }

           fetch('https://shielded-shelf-13155.herokuapp.com/admin/updatecourt', apiOptions)
           .then(res=>res.json())
           .then(res=>{
             let newCourts = courts.map(court=>{
               if(court._id === courtToEdit._id){
                 return res;
               }
               return court;
             });
             setCourts(newCourts);
             setIsLoading(false);
           });
             
      }else{
       const apiOptions ={
         method: "POST",
         headers: {"Content-type":"application/json"},
         body: JSON.stringify({
           courtName,
           courtImg,
           surface: surface._id,
           location: location._id,
           price: parseFloat(price)
         })
       }

       fetch('https://shielded-shelf-13155.herokuapp.com/admin/addcourt', apiOptions)
       .then(res=>res.json())
       .then(res=>{
         res.surface = surface;
         res.location=location;
         const newCourts=[res, ...courts]
         setCourts(newCourts);
       });
      }

       setShowForm(!showForm);
       setCourtToEdit({});
       setIsEditing(false);
       setIsLoading(false);
   }


    // delete Court
    const deleteCourt=(id)=>{
      let apiOptions = {
        method: "DELETE",
        headers:{'Content-type':'application/json'},
        body: JSON.stringify({id})
      }

      fetch('https://shielded-shelf-13155.herokuapp.com/admin/deletecourt', apiOptions)
      .then(res=>res.json())
      .then(res=>{
        // console.log(res)
        const newCourts = courts.filter(court=>{
          return court._id !== id
        });
        setCourts(newCourts);
      });
    }

    if(isLoading) return <Loader/>
    return(
        <div>
        <NavBar/>
        <div className="d-flex justify-content-center py-5">
          <Container className="mt--7" fluid>
            <Row>
              <Card className="col-lg-8 offset-2">
                <CardHeader className="cardcourt d-flex flex-column justify-content-center align-items-center">
                  <h1>Courts</h1>
                  {/* // This button will alter the state of our showform. */}
                  <Button
                  style={{
                      color:'#282828',
                    backgroundColor: 'orange'
                }}
                    onClick = {()=>{setShowForm(!showForm)
                    setCourtToEdit({})}}
                  >Add Court</Button>
       
                  <CourtForm
                  surfaces={surfaces}
                  locations={locations}
                    showForm = {showForm}
                    setShowForm={setShowForm}
                    saveCourt={saveCourt}
                    courtToEdit = {courtToEdit}
                    isEditing = {isEditing}
                  />
                </CardHeader>
                <CardBody>
                  <Table bordered>
                    <thead>
                      <tr>
                        <th>Court Name</th>
                        <th>Court Description</th>
                        <th>Surface Type</th>
                        <th>Location Name</th>
                        <th>Price</th>
                        <th
                          style={{width:"24%",
                        textAlign:'Center'
                        }}
                        >Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      {/* This is our main array => locations */}
                      {courts.map( court =>{
                      return(
                        // This is our component responsible for the individual item
                        <CourtRow
                          court={court}
                          key={court._id}
                          deleteCourt={deleteCourt}
                          setShowForm={setShowForm}
                          setIsEditing={setIsEditing}
                          setCourtToEdit={setCourtToEdit}
                        />
                      )  
                      })}
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
            </Row>
          </Container>
        </div>
        <Footer/>
        </div>
    )
}

export default Courts;