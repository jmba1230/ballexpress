import React, {useState} from 'react';
import { 
    Modal,
    ModalHeader,
    ModalBody,
    Button,
    FormGroup,
    Label,
    Dropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Input
        } from 'reactstrap';

const CourtForm=({showForm, setShowForm, saveCourt, courtToEdit, isEditing, surfaces, locations})=>{

const [isOpenSurfaces, setIsOpenSurfaces]=useState(false);
const [isOpenLocations, setIsOpenLocations]=useState(false);

  // fields we need
  const [courtName, setCourtName]=useState('');
  const [courtImg, setCourtImg]=useState('');
  const [surface, setSurface]=useState('');
  const [location, setLocation]=useState('');
  const [price, setPrice]=useState(null);

    return(
        <Modal
        isOpen={showForm}
        toggle={()=>setShowForm(!showForm)}
        >
            <ModalHeader
            className="modal-header"
                toggle={()=>setShowForm(!showForm)}
            >
            {isEditing ? "Edit Court": "Add Court"}
            </ModalHeader>
            <ModalBody>
                <FormGroup>
                    <Label>Court Name:</Label>
                    <Input
                        placeholder='Enter court name'
                        type='text'
                        onChange={(e)=>setCourtName(e.target.value)}
                        defaultValue={isEditing ? courtToEdit.courtName: ''}
                    /> 
                </FormGroup>
                <FormGroup>
                    <Label>Court Description:</Label>
                    <Input
                        placeholder='Enter Court Description'
                        type='text'
                        onChange={(e)=>setCourtImg(e.target.value)}
                        defaultValue={isEditing ? courtToEdit.courtImg: ''}
                    />
                    </FormGroup>
                    {/* for surface type */}
                    <FormGroup>
                    <Label>Surface Type:</Label>
                    <Dropdown
                        isOpen = {isOpenSurfaces}
                        toggle = {()=>setIsOpenSurfaces(!isOpenSurfaces)}
                    >
                        <DropdownToggle caret
                        style={{
                            color:'black',
                            backgroundColor: 'orange'}}
                        >
                            { !surface 
                            ?
                            'Choose Surface' 
                            :
                            surface.surfaceType    
                            }
                        </DropdownToggle>
                        <DropdownMenu>
                            { surfaces.map(surface => (
                                <DropdownItem
                                    key = {surface._id}
                                    onClick = {()=>setSurface(surface)}
                                >{ surface.surfaceType }</DropdownItem>
                            ))}
                        </DropdownMenu>
                    </Dropdown>
                </FormGroup>
                    {/* for location */}
                    <FormGroup>
                    <Label>Choose Location:</Label>
                    <Dropdown
                        isOpen = {isOpenLocations}
                        toggle = {()=>setIsOpenLocations(!isOpenLocations)}
                    >
                        <DropdownToggle caret
                            style={{
                                color:'black',
                                backgroundColor: 'orange'}}
                        >
                            { !location 
                            ?
                            'Choose Location' 
                            :
                            location.locationName    
                            }
                        </DropdownToggle>
                        <DropdownMenu>
                            { locations.map(location => (
                                <DropdownItem
                                    key = {location._id}
                                    onClick = {()=>setLocation(location)}
                                >{ location.locationName }</DropdownItem>
                            ))}
                        </DropdownMenu>
                    </Dropdown>
                </FormGroup>
                <FormGroup>
                    <Label>Price:</Label>
                    <Input
                    placeholder='Enter Price'
                    type='number'
                    onChange={(e)=>setPrice(e.target.value)}
                    defaultValue={isEditing ? courtToEdit.price: ''}
                    />
                </FormGroup>
                <Button
                    style={{
                        color:'black',
                        backgroundColor:'orange'
                    }}
                    onClick={()=>{
                        saveCourt(courtName, courtImg, surface, location, price);
                        setCourtName('');
                        setCourtImg('');
                        setSurface('');
                        setLocation('');
                        setPrice(null);
                        
                    }}
                >{isEditing ? 'Update Court': 'Save Court'}</Button>
            </ModalBody>
        </Modal>
    )
}

export default CourtForm;