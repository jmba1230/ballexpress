import React, { useState, useEffect } from 'react';
import NavBar from '../../components/NavBar';
import {Container, Row, Card, CardHeader,Button,CardBody, Table} from 'reactstrap';
import SurfaceRow from './SurfaceRow';
import SurfaceForm from './SurfaceForm';
import Footer from '../../components/Footer';
import Loader from '../../components/Loader';

const Surfaces=props=>{

    const [surfaces, setSurfaces]=useState([]);

    // this state will show/hide the add form
    const [showForm, setShowForm]=useState(false);

    // This state tells t he app if we are editing an item or not
    const [isEditing, setIsEditing]=useState(false);
    const [surfaceToEdit, setSurfaceToEdit]=useState({});
    const [allSurfaces, setAllSurfaces]=useState([]);
    const [isLoading, setIsLoading]=useState(false);

    // retrieve location data using useEffect
    useEffect(()=>{

        // retrieval of all locations
        fetch('https://shielded-shelf-13155.herokuapp.com/admin/surfaces')
        .then(res=>res.json())
        .then(responseFromDB=>{
            // console.log(responseFromDB);
            setSurfaces(responseFromDB);
            setIsLoading(false);
        })
    }, []);

        // save data
        const handleSaveSurface=(surfaceType)=>{
            setIsLoading(true);

            if(isEditing){
                let editingsurfaceType = surfaceType;

                if(surfaceType ==='') editingsurfaceType=surfaceToEdit.surfaceType;

                const apiOptions ={
                    method: "PATCH",
                    headers: {'Content-Type': 'application/json'},
                    body: JSON.stringify({
                        id: surfaceToEdit._id,
                        surfaceType: editingsurfaceType
                    })
                }

                fetch('https://shielded-shelf-13155.herokuapp.com/admin/updatesurface', apiOptions)
                .then(res=>res.json())
                .then(res=>{
                    let newSurfaces = surfaces.map(surface=>{
                        if(surface._id === surfaceToEdit._id){
                            return res;
                        }
                        return surface;
                    });
                    setSurfaces(newSurfaces);
                    setIsLoading(false);
                });
            }else{
                let apiOptions ={
                    method: "POST",
                    headers: {'Content-Type': 'application/json'},
                    body: JSON.stringify({
                        surfaceType,
                        // token: sessionStorage.token
                    })
                }

                // save in the database.
                fetch('https://shielded-shelf-13155.herokuapp.com/admin/addsurface', apiOptions)
                .then(res=>res.json())
                .then(res=>{
                    console.log(res);
                    setIsLoading(false);

                    // we need to add the newly created data in our old array of data. (This will update UI)
                setSurfaces([...surfaces,res])
                });
            }
            setShowForm(false);
            setSurfaceToEdit({});
            setIsEditing(false);
        }

        // delete function expecting an id
        // id will tell us which item we want to delete.
        const handleDeleteSurface=(id)=>{
           
            let apiOptions ={
                method: "DELETE",
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify({
                    id
                    // token: sessionStorage.token
                })
            }
        

        // Delete item in our database via fetch
        fetch('https://shielded-shelf-13155.herokuapp.com/admin/deletesurface', apiOptions)
        .then(res=>res.json())
        .then(res=>{
            console.log(res);//this is the deleted item
                // filter the old array, (locations), and return the items where id is not equal to the id of the deleted item
                const newSurfaces = surfaces.filter(surface=>{
                    return surface._id !== id
                });
                setSurfaces(newSurfaces);
            })
            // .catch
        }

    if(isLoading) return <Loader/>
    return(
        <div>
        <NavBar/>
        <div className="d-flex justify-content-center py-5">
          <Container className="mt--7" fluid>
            <Row>
              <Card className="col-lg-8 offset-2">
                <CardHeader className="cardsurface d-flex flex-column justify-content-center align-items-center">
                  <h1
                    className='text-white'
                  >Surfaces</h1>
                  {/* // This button will alter the state of our showform. */}
                  <Button
                  style={{
                      color:'white',
                    backgroundColor: 'orange'
                }}
                    onClick = {()=>setShowForm(!showForm)}
                    
                  >Add Surface</Button>

                  {/* // This is a modal, so technically, you can put this anywhere in your return block */}
                  <SurfaceForm
                    // send the showForm state as a property called showForm
                    // keyName = value
                    showForm = {showForm}

                    // this refers to the method we defined above in useState
                    setShowForm = {setShowForm}

                    // this refers to the saving function
                    handleSaveSurface = {handleSaveSurface}
                    surfaceToEdit = {surfaceToEdit}
                    // this is our signal if we are editing or not
                    isEditing = {isEditing}
                  />
                </CardHeader>
                <CardBody>
                  <Table bordered>
                    <thead>
                      <tr>
                        <th>Surface Type</th>
                        <th
                          style={{width:"24%"}}
                        ></th>
                      </tr>
                    </thead>
                    <tbody>
                      {/* This is our main array => locations */}
                      {surfaces.map( surface =>{
                      return(
                        // This is our component responsible for the individual item
                        <SurfaceRow
                          key={surface._id}
                          surface={surface}
                        // This is the delete function
                          handleDeleteSurface={handleDeleteSurface}
                          setShowForm={setShowForm}
                          setIsEditing={setIsEditing}
                          setSurfaceToEdit={setSurfaceToEdit}
                        />
                      )  
                      })}
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
            </Row>
          </Container>
        </div>
        <Footer/>
        </div>
    )
}
export default Surfaces;