import React, {useState} from 'react';
import {Modal, ModalHeader, ModalBody, FormGroup, Label, Input,Button} from 'reactstrap';

// this component will receive props, and will use the "Key names" of the props received

const SurfaceForm=({showForm, setShowForm,handleSaveSurface,surfaceToEdit,isEditing})=>{
    
    // created state to temporary save the data from input fields
    const [surfaceType, setSurfaceType]=useState('');


    // In our modal, the initial value of isOpen is false. (Which is the default value of showForm.)

    // When a user clicks the add button, the state will change. showForm is not equal to true.

    // Since there is a state change (showForm), the app will re-render.

    return(
        <Modal
            isOpen={showForm}
            toggle={()=>setShowForm(!showForm)}
        >
            <ModalHeader
            className="modal-header"
                toggle={()=>setShowForm(!showForm)}
            >
            {isEditing ? "Edit Surface": "Add Surface"}
            </ModalHeader>
            <ModalBody>
                <FormGroup>
                    <Label>Surface Name:</Label>
                    <Input
                        placeholder='Name'

                        // use onChange function to capture data in input
                        onChange={e=>setSurfaceType(e.target.value)}
                        defaultValue={isEditing ? surfaceToEdit.surfaceType:''}
                    />
                </FormGroup>
                <Button
                color='warning'
                block
                onClick={()=>{
                    // This function came from the Locations.js and we received it as a prop.
                        // This function expects 2 arguments.
                        // The arguments name and location came from the state of this component.
                        handleSaveSurface(surfaceType);
                        setSurfaceType("");

                }}
             >{isEditing ? "Update Surface":"Save Surface"}</Button>
            </ModalBody>
        </Modal>
    )
}

export default SurfaceForm;