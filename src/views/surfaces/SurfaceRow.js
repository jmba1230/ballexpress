import React from 'react';
import {Button} from 'reactstrap';

const SurfaceRow=({
    surface,
    handleDeleteSurface,
    setShowForm,
    setIsEditing,
    setSurfaceToEdit,
})=>{
    return(
        <tr>
            <td>{surface.surfaceType}</td>
            <td>
                <Button
                     style={{
                        color:'white',
                      backgroundColor: 'orange'
                  }}
                    onClick={()=>{
                        // This is to show the form
                        setShowForm(true);
                        // this is the way of telling app that we are editing
                        setIsEditing(true);
                        // This is the item we want to edit
                        // Save the whole data (old value)
                        setSurfaceToEdit(surface);
                    }}
                >Edit</Button>{' '}
                <Button
                     style={{
                        color:'white',
                      backgroundColor: 'orange'
                  }}
                    onClick={()=>handleDeleteSurface(surface._id)}
                >Delete</Button>
            </td>
        </tr>
    )
}

export default SurfaceRow;