import React, { useState, useEffect } from 'react';
import NavBar from '../../components/NavBar';
import {Container, Row, Card, CardHeader,Button,CardBody, Table} from 'reactstrap';
import LocationRow from './LocationRow';
import LocationForm from './LocationForm';
import Footer from '../../components/Footer';
import Loader from '../../components/Loader';

const Locations=props=>{

    const [locations, setLocations]=useState([]);

    // this state will show/hide the add form
    const [showForm, setShowForm]=useState(false);

    // This state tells t he app if we are editing an item or not
    const [isEditing, setIsEditing]=useState(false);
    const [locationToEdit, setLocationToEdit]=useState({});

    const [isLoading, setIsLoading]=useState(false);

    // retrieve location data using useEffect
    useEffect(()=>{

        // retrieval of all locations
        fetch('https://shielded-shelf-13155.herokuapp.com/admin/locations')
        .then(res=>res.json())
        .then(responseFromDB=>{
            // console.log(responseFromDB);
            setLocations(responseFromDB);
            setIsLoading(false);
        })
    }, []);

        // save data
        const handleSaveLocation=(locationName, locationCity)=>{
            setIsLoading(true);

            if(isEditing){
                let editinglocationName = locationName;
                let editinglocationCity = locationCity;

                if(locationName==='') editinglocationName=locationToEdit.locationName;
                if(locationCity==='') editinglocationCity=locationToEdit.locationCity;

                const apiOptions ={
                    method: "PATCH",
                    headers: {'Content-Type': 'application/json'},
                    body: JSON.stringify({
                        id: locationToEdit._id,
                        locationName: editinglocationName,
                        locationCity: editinglocationCity
                    })
                }

                fetch('https://shielded-shelf-13155.herokuapp.com/admin/updatelocation', apiOptions)
                .then(res=>res.json())
                .then(res=>{
                    let newLocations = locations.map(location=>{
                        if(location._id === locationToEdit._id){
                            return res;
                        }
                        return location;
                    });
                    setLocations(newLocations);
                    setIsLoading(false);
                });
            }else{
                let apiOptions ={
                    method: "POST",
                    headers: {'Content-Type': 'application/json'},
                    body: JSON.stringify({
                        locationName,
                        locationCity
                        // token: sessionStorage.token
                    })
                }

                // save in the database.
                fetch('https://shielded-shelf-13155.herokuapp.com/admin/addlocation', apiOptions)
                .then(res=>res.json())
                .then(res=>{
                    console.log(res);
                    setIsLoading(false);

                    // we need to add the newly created data in our old array of data. (This will update UI)
                setLocations([...locations,res])
                });
            }
            setShowForm(false);
            setLocationToEdit({});
            setIsEditing(false);
        }

        // delete function expecting an id
        // id will tell us which item we want to delete.
        const handleDeleteLocation=(id)=>{
           
            let apiOptions ={
                method: "DELETE",
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify({
                    id
                    // token: sessionStorage.token
                })
            }
        
        // Delete item in our database via fetch
        fetch('https://shielded-shelf-13155.herokuapp.com/admin/deletelocation', apiOptions)
        .then(res=>res.json())
        .then(res=>{
            // console.log(res);//this is the deleted item
                // filter the old array, (locations), and return the items where id is not equal to the id of the deleted item
                const newLocations = locations.filter(location=>{
                    return location._id !== id
                });
                setLocations(newLocations);
            })
            // .catch
        }

    if(isLoading) return <Loader/>
    return(
        <div>
        <NavBar/>
        <div className="d-flex justify-content-center py-5">
          <Container className="mt--7" fluid>
            <Row>
              <Card className="col-lg-8 offset-2">
                <CardHeader className="cardlocation d-flex flex-column justify-content-center align-items-center">
                  <h1
                    className='text-white'
                  >Locations</h1>
                  {/* // This button will alter the state of our showform. */}
                  <Button
                  style={{
                      color:'#282828',
                    backgroundColor: 'orange'
                }}
                    onClick = {()=>setShowForm(!showForm)}
                    
                  >Add Location</Button>
                  {/* // This is a modal, so technically, you can put this anywhere in your return block */}
                  <LocationForm
                    // send the showForm state as a property called showForm
                    // keyName = value
                    showForm = {showForm}

                    // this refers to the method we defined above in useState
                    setShowForm = {setShowForm}

                    // this refers to the saving function
                    handleSaveLocation = {handleSaveLocation}
                    locationToEdit = {locationToEdit}
                    // this is our signal if we are editing or not
                    isEditing = {isEditing}
                  />
                </CardHeader>
                <CardBody>
                  <Table bordered>
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>City</th>
                        <th
                          style={{width:"24%"}}
                        ></th>
                      </tr>
                    </thead>
                    <tbody>
                      {/* This is our main array => locations */}
                      {locations.map( location =>{
                      return(
                        // This is our component responsible for the individual item
                        <LocationRow
                          key={location._id}
                          location={location}
                        // This is the delete function
                          handleDeleteLocation={handleDeleteLocation}
                          setShowForm={setShowForm}
                          setIsEditing={setIsEditing}
                          setLocationToEdit={setLocationToEdit}
                        />
                      )  
                      })}
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
            </Row>
          </Container>
        </div>
        <Footer/>
        </div>
    )
}

export default Locations;