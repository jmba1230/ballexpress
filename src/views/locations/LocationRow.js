import React from 'react';
import {Button} from 'reactstrap';

const LocationRow=({
    location,
    handleDeleteLocation,
    setShowForm,
    setIsEditing,
    setLocationToEdit,
})=>{
    return(
        <tr>
            <td>{location.locationName}</td>
            <td>{location.locationCity}</td>
            <td>
                <Button
                     style={{
                        color:'#282828',
                      backgroundColor: 'orange'
                  }}
                    onClick={()=>{
                        // This is to show the form
                        setShowForm(true);
                        // this is the way of telling app that we are editing
                        setIsEditing(true);
                        // This is the item we want to edit
                        // Save the whole data (old value)
                        setLocationToEdit(location);
                    }}
                >Edit</Button>{' '}
                <Button
                     style={{
                        color:'#282828',
                      backgroundColor: 'orange'
                  }}
                    onClick={()=>handleDeleteLocation(location._id)}
                >Delete</Button>
            </td>
        </tr>
    )
}

export default LocationRow;