import React, {useState} from 'react';
import {Modal, ModalHeader, ModalBody, FormGroup, Label, Input,Button} from 'reactstrap';

// this component will receive props, and will use the "Key names" of the props received

const LocationForm=({showForm, setShowForm,handleSaveLocation,locationToEdit,isEditing})=>{
    
    // created state to temporary save the data from input fields
    const [locationName, setLocationName]=useState('');
    const [locationCity, setLocationCity]=useState('');


    // In our modal, the initial value of isOpen is false. (Which is the default value of showForm.)

    // When a user clicks the add button, the state will change. showForm is not equal to true.

    // Since there is a state change (showForm), the app will re-render.

    return(
        <Modal
            isOpen={showForm}
            toggle={()=>setShowForm(!showForm)}
        >
            <ModalHeader
                toggle={()=>setShowForm(!showForm)}
            >
            {isEditing ? "Edit Location": "Add Location"}
            </ModalHeader>
            <ModalBody>
                <FormGroup>
                    <Label>Location Name:</Label>
                    <Input
                        placeholder='Name'

                        // use onChange function to capture data in input
                        onChange={e=>setLocationName(e.target.value)}
                        defaultValue={isEditing ? locationToEdit.locationName:''}
                    />
                </FormGroup>
                <FormGroup>
                    <Label>City:</Label>
                    <Input
                        placeholder='City'
                        onChange={e=>setLocationCity(e.target.value)}
                        defaultValue={isEditing ? locationToEdit.locationCity: ''}
                    />
                </FormGroup>
                <Button
                style={{
                    backgroundColor:'orange'
                }}
                block
                onClick={()=>{
                    // This function came from the Locations.js and we received it as a prop.
                        // This function expects 2 arguments.
                        // The arguments name and location came from the state of this component.
                        handleSaveLocation(locationName, locationCity);
                        setLocationName("");
                        setLocationCity("");

                }}
             >{isEditing ? "Update Location":"Save Location"}</Button>
            </ModalBody>
        </Modal>
    )
}

export default LocationForm;