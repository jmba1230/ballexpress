import React, {useState} from 'react';
import {Button, Modal, ModalHeader, ModalBody} from 'reactstrap';
import moment from 'moment';
import PaymentForm from './PaymentForm';

import {loadStripe} from '@stripe/stripe-js';
import {Elements} from '@stripe/react-stripe-js';

const stripePromise = loadStripe('pk_test_8iIVu3Qt4Fy29ZbiuBORCTFB00ZgTbdIT5');

const ScheduleRow=({
    schedule,
    setRefresh,
    refresh
})=>{
   
    const [showPaymentForm, setShowPaymentForm]=useState(false);
    const [scheduleIdToPay, setScheduleIdToPay]=useState([]);

    return(
        <>
        <tr>
            <td>{schedule.court?.courtName}</td>
            <td>{moment(schedule.startDate).format('ll')} - {moment(schedule.endDate).format('ll')}</td>
            <td>Php {(schedule.amount) + '.00'}</td>
            <td>{schedule.status}</td>
            <td><Button
            style={{
                backgroundColor: 'orange',
                color: 'whitesmoke'
            }}
            onClick={()=>{setShowPaymentForm(!showPaymentForm)
            setScheduleIdToPay(schedule._id)
            }}
            disabled = {schedule.status === 'Paid via Stripe' || schedule.status ==='Cancelled'}
            >Pay</Button>{' '}
            <Button
            disabled = {schedule.status === 'Paid via Stripe' ? true:false} 
            >Cancel</Button>
            </td>
                
        </tr>
        <Modal
            isOpen = {showPaymentForm}
            toggle = { () => setShowPaymentForm(false)}
        >
            <ModalHeader
            style={{
                backgroundColor: 'orange',
                color: 'white'
            }}
                toggle = { () => setShowPaymentForm(false)}
            >
                Payment
            </ModalHeader>
            <ModalBody>              
                <Elements stripe={stripePromise}>
                <PaymentForm
                    setShowPaymentForm={setShowPaymentForm}
                    scheduleIdToPay={scheduleIdToPay}
                    setRefresh={setRefresh}
                    refresh={refresh}
                />
                </Elements>
            </ModalBody>
        </Modal>
        </>
    )

}

export default ScheduleRow;