import React, { useState, useEffect } from 'react';
import DayPicker, {DateUtils} from 'react-day-picker';
import 'react-day-picker/lib/style.css';
import { ErrorToast } from '../../components/Toasts';


const ReactDayPicker = ({dateRange, setDateRange, court}) => {


    const initialState = { from: null, to: null, enteredTo: null }
    const [blockedDates, setBlockedDates] = useState([]);
    const [schedules, setSchedules]=useState([]);
    const [pikachuArray, setPikachuArray] = useState([]);
    
    useEffect(()=>{
        
            fetch('https://shielded-shelf-13155.herokuapp.com/admin/schedules/' +court._id)
            .then(res=>res.json())
            .then(res=>{
                const datesToDisable = res.map(schedule => {
                    return {from: new Date(schedule.startDate), to: new Date(schedule.endDate)}
                });
                
                setBlockedDates(datesToDisable);
            })

    }, [])



    const isSelectingFirstDay = (from, to, day) => {
        const isBeforeFirstDay = from && DateUtils.isDayBefore(day, from);
        const isRangeSelected = from && to;

        return !from ||isBeforeFirstDay || isRangeSelected;
    }

    const handleDayClick = (day, {disabled}) => {
        // console.log(pikachu)
        if(disabled) return;

        const {from, to} = dateRange;
        if(from && to && day >= from && day <=to){
            handleResetClick()
            return;
        }
        if(isSelectingFirstDay(from, to, day)) {
            setDateRange({
                from: day,
                to: null,
                enteredTo: null
            })
        }else {

            const startDate = from.toString();
            const dates = blockedDates

            let isAvailable = true;
            

            while(+from <= +day){
                dates.forEach((date)=>{
                    const range = DateUtils.isDayInRange(from, date)
                    if (range){
                        isAvailable = false;
                    }

                })


                // if(dates.some()) isAvailable=false
                from.setDate(from.getDate()+1)
            }

            if(!isAvailable){

                ErrorToast('Invalid Booking! System will not accept dates in between!')
                return setDateRange(initialState)
            }

            setDateRange({
                from:new Date(startDate),
                to: day,
                enteredTo: day
            })
        }
    }

    // const handleDayMouseEnter = (day) => {
    //     const {from, to} = dateRange;
    //     if(!isSelectingFirstDay(from, to, day)){
    //         setDateRange({
    //             from,
    //             to,
    //             enteredTo: day
    //         })
    //     }
    // }

    const handleResetClick = () => {
        setDateRange(initialState);
    }

    const {from, enteredTo} = dateRange;

    return(
        <>
            
            <DayPicker 
                numberOfMonths = {1}
                fromMonth = {from}
                selectedDays = {[from, {from, to: enteredTo}]}
                disabledDays = { [...blockedDates, {before: new Date()}]}
                modifiers = {{ start: from, end: enteredTo }}
                onDayClick = {handleDayClick}
         
            />
            <div> 
                {!dateRange.from && !dateRange.to && 'Please select the first day.'}
                {dateRange.from && !dateRange.to && 'Please select the last day.'}
                {dateRange.from && 
                    dateRange.to && 
                        `${dateRange.from.toLocaleDateString()} to ${dateRange.to.toLocaleDateString()}`}{' '} {dateRange.from && dateRange.to && ( 
                        
                    <button className="link" onClick={handleResetClick}> Reset </button> 
                )} 
            </div>
        </>
    )
}

export default ReactDayPicker;
