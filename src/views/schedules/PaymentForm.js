import React from 'react';
import {useStripe, useElements, CardElement} from '@stripe/react-stripe-js';
import {SuccessToast, ErrorToast} from '../../components/Toasts';

const PaymentForm = ({setShowPaymentForm, scheduleIdToPay,setRefresh, refresh}) => {
    const stripe = useStripe();
    const elements = useElements();

    const stripePay=async()=>{

        let res = await fetch('https://shielded-shelf-13155.herokuapp.com/admin/secret/' +scheduleIdToPay)
        const {client_secret}=await res.json()
        console.log(client_secret);
        const result = await stripe.confirmCardPayment(client_secret, {
            payment_method: {
              card: elements.getElement(CardElement),
              billing_details: {
                name: sessionStorage.userName,
              },
            }
          });

          setShowPaymentForm(false);

          if (result.error) {
            // Show error to your customer (e.g., insufficient funds)
            ErrorToast('Payment is declined!')
          } else {
            // The payment has been processed!
            if (result.paymentIntent.status === 'succeeded') {
                SuccessToast('Payment has been approved!')
                setRefresh(!refresh)
              // Show a success message to your customer
              // There's a risk of the customer closing the window before callback
              // execution. Set up a webhook or plugin to listen for the
              // payment_intent.succeeded event that handles any business critical
              // post-payment actions.
            }
          }
    }    
    
    return(
        <>
<CardElement
 options={{
    style: {
      base: {
        fontSize: '16px',
        color: '#424770',
        '::placeholder': {
          color: '#aab7c4',
        },
      },
      invalid: {
        color: '#9e2146',
      },
    },
  }}
/> 
<button className="mt-3"
                    style={{
                        backgroundColor: 'orange',
                        color: 'whitesmoke'
                    }}
                    onClick={stripePay}
                  >Submit</button>
</>

    )};

export default PaymentForm;