import React, { useState, useEffect } from 'react';
import { Modal, ModalHeader, ModalBody, Card, CardHeader, CardImg, Label, CardBody, CardTitle, CardSubtitle, FormGroup, Input, Button } from 'reactstrap';
import ReactDayPicker from './ReactDayPicker';
import { SuccessToast } from '../../components/Toasts';


const ScheduleForm = ({showScheduleForm, toggleScheduleForm, court}) => {
   

    const [schedules, setSchedules] = useState([]);
    const [courts, setCourts] = useState([]);
    const [locations, setLocations] = useState([]);
    const [surface, setSurfaces] = useState([]);
    const [blockedDates, setBlockedDates] = useState([]);

    // files we need
   
    const [user, setUser] = useState(null);
    const [startDate, setStartDate] = useState('');
    const [endDate, setEndDate] = useState('');
    const [amount, setAmount] = useState(0);
    const [dateRange, setDateRange] = useState({ from: null, to: null, enteredTo: null });

    
    useEffect(() => {
        fetch('https://shielded-shelf-13155.herokuapp.com/admin/courts')
        .then(res => res.json())
        .then(res => {
            
            setCourts(res);
        })

    }, [])

    
    const handleSaveBooking = () => {


        let {from, to} = dateRange;
        if (to === null) {
            to = from
        }
        const dateDiff = (to.getDate() - from.getDate()) + 1;
              
        const amount = court.price * dateDiff;


        const apiOptions = {
            method: 'POST',
            headers: {'Content-type' : 'application/json'},
            body: JSON.stringify({
                court: court._id,
                user: sessionStorage.userId,
                startDate: from,
                endDate: to,
                amount
            })
        }

        fetch('https://shielded-shelf-13155.herokuapp.com/admin/addschedule', apiOptions)
        .then(res => res.json())
        .then(res => {
            res.court = court;
            res.user = sessionStorage.userId;

            SuccessToast('Successfully booked. Pay now.')

            const newSchedules = [res, ...schedules];
            setSchedules(newSchedules);
        })

        toggleScheduleForm();
       
    }

    return(
        <>          

            <Modal
                isOpen = {showScheduleForm}
                toggle = {toggleScheduleForm}
            >
                <ModalHeader
                    toggle = {toggleScheduleForm}
                    style = {{
                        fontWeight: 'bolder',
                        textTransform: 'uppercase',
                        backgroundColor: 'orange',
                        color: 'white'
                    }}
                >
                    Book & Get Ready to Play Ball
                </ModalHeader>
                <ModalBody>
                    
                        <Card
                            key = {court._id}
                        >
                            <CardHeader
                                style = {{
                                    backgroundColor:'orange',
                                    fontWeight: 'bolder',
                                    textTransform: 'uppercase',
                                    padding: 0
                                }}
                            >  
                                <CardImg 
                                style = {{
                                    height: '25%',
                                    width: '25%'
                                }}
                            />{court.name}
                            </CardHeader>
                            <CardBody
                                className = 'text-center'
                            >
                                <CardTitle
                                    style = {{
                                        fontSize: '20px',
                                        margin: '1px'
                                    }}
                                >Surface Type: {court.surface?.surfaceType} </CardTitle>
                                <CardSubtitle
                                    style = {{
                                       fontSize: '20px',
                                        margin: '1px'
                                    }}
                                >
                                    Location: {court.location?.locationName}
                                </CardSubtitle>
                                <CardSubtitle
                                    style = {{
                                        color: 'orange',
                                        margin: '1px',
                                        fontWeight: 'bolder',
                                        fontSize: '20px'
                                    }}
                                >
                                    Price: Php {court.price}
                                </CardSubtitle>
                                <FormGroup>
                                    <Label
                                        className = 'm-3'
                                        style = {{
                                            textDecoration: 'underline',
                                            fontWeight: 'bold',
                                            color: 'darkred'
                                        }}
                                    >Available Dates:</Label>
                                    <ReactDayPicker
                                        dateRange = {dateRange} 
                                        setDateRange = {setDateRange}
                                        court = {court} 
                                    />
                                </FormGroup>
                                
                                <Button
                                style={{
                                    color:'#282828',
                                    backgroundColor: 'orange'
                                }}
                                    onClick = {handleSaveBooking}
                                >Book</Button>
                            </CardBody>
                        </Card>
                            
                </ModalBody>
            </Modal>
        </>
    )
}

export default ScheduleForm;
