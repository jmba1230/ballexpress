import React, {useEffect, useState} from 'react';
import {Container, Row, Card, CardHeader, CardBody, Table} from 'reactstrap';
import ScheduleRow from './ScheduleRow';
import NavBar from '../../components/NavBar';
import Footer from '../../components/Footer';

const Schedule=props=>{

    const [schedules, setSchedules]=useState([]);
    const [refresh,setRefresh]=useState(false)

    useEffect(()=>{
        fetch('https://shielded-shelf-13155.herokuapp.com/admin/schedules/' +sessionStorage.userId)
        .then(res => res.json())
        .then(res=>{
            setSchedules(res);
        })
    },[refresh])

    return(
        <>
            <NavBar/>
            <div className="d-flex justify-content-center py-5">
          <Container className="mt--7" fluid>
            <Row>
              <Card className="col-lg-8 offset-2">
                <CardHeader className="cardschedule d-flex flex-column justify-content-center align-items-center">
                  <h1
                    className='text-white'
                  >Bookings</h1>
                </CardHeader>
                <CardBody>
                  <Table bordered>
                    <thead>
                      <tr>
                        <th>Court Name</th>
                        <th>Start Date - End Date</th>
                        <th>Amount</th>
                        <th>Status</th>
                        <th
                          style={{width:"18%"}}
                        >Action</th>
                      </tr>
                    </thead>
                    <tbody>
                     {/* This is our main array => locations */}
                     {schedules.map( schedule =>{
                      return(
                        // This is our component responsible for the individual item
                        <ScheduleRow
                          key={schedule._id}
                          schedule={schedule}
                          setRefresh={setRefresh}
                          refresh={refresh}
                        />
                      )  
                      })}
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
            </Row>
          </Container>
        </div>
            <Footer/>
        </>
    )
}

export default Schedule