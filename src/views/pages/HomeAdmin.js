import React, {useEffect, useState} from 'react';
import {Container, Row, Card, CardHeader, CardBody, Table} from 'reactstrap';
import NavBar from '../../components/NavBar';
import Footer from '../../components/Footer';
import moment from 'moment';

const HomeAdmin=props=>{

    const [schedules, setSchedules]=useState([]);

    useEffect(()=>{
        fetch('https://shielded-shelf-13155.herokuapp.com/admin/schedules')
        .then(res => res.json())
        .then(res=>{
            setSchedules(res);
        })
    },[])

    return(
        <>
            <NavBar/>
            <div className="d-flex justify-content-center py-5">
          <Container className="mt--7" fluid>
            <Row>
              <Card className="col-lg-8 offset-2">
                <CardHeader className="cardschedule d-flex flex-column justify-content-center align-items-center">
                  <h1
                    className='text-white'
                  >Court Bookings</h1>
                </CardHeader>
                <CardBody>
                  <Table bordered>
                    <thead>
                      <tr>
                        <th>Court Name</th>
                        <th>Start Date - End Date</th>
                        <th>Amount</th>
                        <th>Status</th>
                      </tr>
                    </thead>
                    <tbody>
                     {/* This is our main array => schedules */}
                     {schedules.map( schedule =>{
                      return(
                        <tr>
                            <td>{schedule.court?.courtName}</td>
                            <td>{moment(schedule.startDate).format('ll')} - {moment(schedule.endDate).format('ll')}</td>
                            <td>Php {(schedule.amount) + '.00'}</td>
                            <td>{schedule.status}</td>
                        </tr>
                      )  
                      })}
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
            </Row>
          </Container>
        </div>
            <Footer/>
        </>
    )
}

export default HomeAdmin;