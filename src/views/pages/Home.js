import React, { useEffect, useState } from 'react';
import NavBar from '../../components/NavBar';
import Footer from '../../components/Footer';
import { Card, CardHeader, CardBody, Table, Button} from 'reactstrap';
import Loader from '../../components/Loader';
import ScheduleForm from '../schedules/ScheduleForm'


const Home=props=>{

    const [courts, setCourts]=useState([]);
    const [court, setCourt]=useState({});

    const [showForm, setShowForm] = useState(false);
    const [showScheduleForm, setShowScheduleForm] = useState(false);
    const [bookCourt, setBookCourt]=useState({});

    const [isLoading, setIsLoading]=useState(false);

    useEffect(()=>{
    fetch('https://shielded-shelf-13155.herokuapp.com/admin/courts')
    .then(res=>res.json())
    .then(responseFromDB=>{
        // console.log(responseFromDB);
        setCourts(responseFromDB);
        setIsLoading(false);
    })
}, []);


const handleBookingForm = (court) =>{
    setBookCourt(court);
    setShowScheduleForm(true);
}    

const toggleScheduleForm = () => {
    setShowScheduleForm(!showScheduleForm);
}


    if(isLoading) return <Loader/>;
    return( 
        <div>
        <NavBar/>
        <div className='container'>
        <div className='row d-flex align-items-center justify-content-center'>
        <div className='col-md-12 py-5'>
            <Card className='cardavailablecourt'>
                <CardHeader>
                    <h4
                        style={{
                            color:'white'
                        }}
                    >Available Courts</h4>
                </CardHeader>
                <CardBody>
                    <Table striped
                        style={{
                            color:'white'
                        }}
                    > 
                        <thead>
                        <tr>
                            <th>Court Name</th>
                            <th>Surface Type</th>
                            <th>Location</th>
                            <th>Price</th>
                            <th>BOOK NOW!!</th>
                        </tr>
                        </thead>
                        <tbody>
                            {courts.map(court=>{
                                return(
                                    <tr
                                        key={court._id}
                                    >
                                        <td>{court.courtName}</td>
                                        <td>{court.surface?.surfaceType}</td>
                                        <td>{court.location.locationName}</td>
                                        <td>Php {(court.price)+ '.00'}</td>
                                        <td>
                                         <Button
                                            style  ={{
                                                backgroundColor: 'orange'
                                            }}
                                            onClick ={()=>handleBookingForm(court)} 
                                            
                                        >BOOK</Button>
                                        
                                        </td>
                                        

                                    </tr>
                                    
                                )

                            })}
                        </tbody>
                        <ScheduleForm 
                                            showScheduleForm = {showScheduleForm}
                                            toggleScheduleForm = {toggleScheduleForm}
                                            court={bookCourt}
                                            />
                    </Table>

                </CardBody>
            </Card>
        </div>
        </div>
        </div>
        <Footer/>
        
        </div>
    )
}

export default Home;