import React, { useState, useEffect } from 'react';
import { FormGroup, Label, Input, Button} from 'reactstrap';
import { ErrorToast, SuccessToast } from '../../components/Toasts';
import {Link, useHistory} from 'react-router-dom';
import Footer from '../../components/Footer';

const Login = props => {

    useEffect(()=>{
        sessionStorage.clear();
    },[])

    const [ email, setEmail ] = useState('');
    const [ password, setPassword ] = useState('');

    const {push} = useHistory()

    const handleLogin = async() => {
        try{
        const apiOptions = {
            method: "POST",
            headers: {'Content-type': 'application/json'},
            body: JSON.stringify({
                email,
                password
            })
        }

        const fetchedData = await fetch('https://shielded-shelf-13155.herokuapp.com/login', apiOptions);
        const data = await fetchedData.json();
        console.log(data);

        sessionStorage.token = data.token;
        sessionStorage.userId = data.user.id;
        sessionStorage.isAdmin = data.user.isAdmin;
        sessionStorage.userName = data.user.firstName + " " + data.user.lastName;
        sessionStorage.email = data.user.email;

        // window.location.replace('/');
        if(sessionStorage.isAdmin === 'true'){
        push('/homeadmin');
        SuccessToast('Login Successful');
        }else
        {
        push('/home');
        SuccessToast('Login Successful');
        }
    }catch{
        ErrorToast('Unuathorized account. Please check your details/kindly register.')
    }
    }

    return (
        <div
            className='login d-flex justify-content-center align-items-center vh-100 py-5'
        >
            <div className="landing-title">
                <em style={{fontWeight:900, fontSize:'2.5rem'}}>ball<span style={{color:'orange'}}>Express</span></em>
           </div>
            <Link to='/'><div class="ball"></div></Link>
            <div style={{width: '40vw'}} className='loginform'>
                <h1 className='text-center text-white'>Login</h1>
                <form>
                    <FormGroup>
                        <Label style={{color:'white'}}>Email:</Label>
                        <Input 
                            type = 'email'
                            placeholder = 'Enter your email address'
                            onChange = { (e) => setEmail(e.target.value) }
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label style={{color:'white'}}>Password:</Label>
                        <Input 
                            type='password'
                            placeholder = 'Enter your password'
                            onChange = { (e) => setPassword(e.target.value) }
                        />
                    </FormGroup>
                    <Button 
                        disabled = { email === '' || password === '' ? true : false}
                        onClick = { handleLogin }
                        style={{
                            backgroundColor: 'orange',
                            color:'#282828'
                        }}
                    ><em>Login</em></Button>
                    <p
                        style={{
                            color:'white'
                        }}
                    >Not Registered?</p>
                    <Button
                        style={{
                            backgroundColor: 'orange',
                            color: '#282828'
                        }}
                    >
                    <Link to='/register'
                        style={{
                            color:'#282828'
                        }}
                    ><em>Register here</em></Link>
                    </Button>
                    </form>
            </div>
            <Footer/>
        </div>
    )
}

export default Login;