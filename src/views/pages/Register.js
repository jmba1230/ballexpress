import React, { useState } from 'react';
import { FormGroup, Label, Input, Button } from 'reactstrap';
import {Link} from 'react-router-dom';
import Footer from '../../components/Footer';

const Register = props => {

    const [ firstName, setFirstName ] = useState('');
    const [ lastName, setLastName ] = useState('');
    const [ email, setEmail ] = useState('');
    const [ password, setPassword ] = useState('');
    const [ confirmPassword, setConfirmPassword ] = useState('');

    const handleRegister = async () => {
        const apiOptions = {
            method: "POST",
            headers: {'Content-type': 'application/json'},
            body: JSON.stringify({
                firstName,
                lastName,
                email,
                password
            })
        }

        await fetch('https://shielded-shelf-13155.herokuapp.com/register', apiOptions);

        window.location.replace('/login');
    } 

    return(
        <div
            className='register d-flex justify-content-center align-items-center vh-100'
        >
             <div className="landing-title">
                <em style={{fontWeight:900, fontSize:'2.5rem'}}>ball<span style={{color:'orange'}}>Express</span></em>
           </div>
            <div style={{width: '40vw'}} classname="registerform">
                <h1
                    style={{color:'white'}}
                >Register</h1>
                <form>
                    <FormGroup>
                        <Label
                            style={{color:'white'}}
                        >First Name:</Label>
                        <Input
                            type='text'
                            placeholder='Enter your first name'
                            onChange = { (e) => setFirstName(e.target.value) }
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label
                            style={{color:'white'}}
                        >Last Name:</Label>
                        <Input
                            type='text'
                            placeholder='Enter your last name'
                            onChange = { (e) => setLastName(e.target.value) }
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label
                            style={{color:'white'}}
                        >Email:</Label>
                        <Input
                            type='email'
                            placeholder='Enter your email address'
                            onChange = { (e) => setEmail(e.target.value) }
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label
                            style={{color:'white'}}
                        >Password:</Label>
                        <Input
                            type='password'
                            placeholder='Enter your password'
                            onChange = { (e) => setPassword(e.target.value) }
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label
                            style={{color:'white'}}
                        >Confirm Password:</Label>
                        <Input
                            type='password'
                            placeholder='Confirm password'
                            onChange = { (e) => setConfirmPassword(e.target.value) }
                        />
                        <span className='text-danger'>{ confirmPassword !== '' && confirmPassword !== password ? 'Passwords did not match' : ''}</span>
                    </FormGroup>
                    <Button
                        type='button'
                        color='warning'
                        disabled = { firstName === '' || lastName === '' || email === '' || password === '' || password !== confirmPassword ? true : false }
                        onClick = { handleRegister }
                    >
                        <em>Register</em>
                    </Button>{' '}
                    <Button
                        style={{
                            backgroundColor: 'orange',
                            color: 'white'
                        }}
                    >
                    <Link to='/login'
                        style={{
                            color:'#282828'
                        }}
                    ><em>Back to Login</em></Link>
                    </Button>
                </form>
            </div>
            <Link to='/'><div class="ball"></div></Link>
            <Footer/>
        </div>
    )
}

export default Register;