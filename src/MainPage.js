import React from 'react';

import {
    BrowserRouter, Route, Switch
} from 'react-router-dom';

import Loader from './components/Loader';
import BallExpress from './BallExpress';

// import pages
const Home = React.lazy(()=>import('./views/pages/Home'));

// Home Admin
const HomeAdmin = React.lazy(()=>import('./views/pages/HomeAdmin'));

const Locations = React.lazy(()=>import('./views/locations/Locations'));
const Surfaces = React.lazy(()=>import('./views/surfaces/Surfaces'));
const Courts = React.lazy(()=>import('./views/courts/Courts'));
// for schedule
const Schedules = React.lazy(()=>import('./views/schedules/Schedules'));
// for booking
const Login = React.lazy(()=>import('./views/pages/Login'));
const Register = React.lazy(()=>import('./views/pages/Register'));

const MainPage=()=>{
    return(
        <BrowserRouter>
         <React.Suspense
            fallback={<Loader/>}
         >
             <Switch>
                 <Route
                    path='/'
                    exact
                    render={props => <BallExpress {...props} />}
                 />
                 <Route 
                        path ='/home'
                        render = {props => <Home {...props} /> }
                    />
                <Route
                    path='/homeadmin'
                    render={props => <HomeAdmin {...props} />}
                />
                <Route
                    path='/login'
                    render={props => <Login {...props} />}
                />
                <Route
                    path='/register'
                    render={props => <Register {...props} />}
                />
                 <Route
                    path='/locations'
                    render={props => <Locations {...props} />}
                 />
                 <Route
                    path='/surfaces'
                    render={props => <Surfaces {...props} />}
                 />
                 <Route
                    path='/courts'
                    render={props => <Courts {...props} />}
                 />
                 <Route
                    path='/schedule'
                    render={props => <Schedules {...props} />}
                />

             </Switch>  
             </React.Suspense>   
        </BrowserRouter>
    )
}

export default MainPage;