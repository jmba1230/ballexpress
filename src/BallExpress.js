import React from 'react';
import {Link} from 'react-router-dom'
import { Button } from 'reactstrap';

const BallExpress=props=>{
    return(

            <div className='landing d-flex vh-100 justify-content-center align-items-center flex-column'>
                <div className="landing-title">
                <em style={{fontWeight:900, fontSize:'2.5rem'}}>ball<span style={{color:'orange'}}>Express</span></em>
           </div>
           <div className='landing-cta'>
               <h2>Ball is Life! Book Now!</h2>
           </div>
                <div className="mx-auto py-3">
           <Button
            style={{
                backgroundColor: 'orange'
            }}
           >
               <Link to='/login'
                style={{
                    color:'#282828'
                }}
               ><em>Click Here to Login / Register</em></Link>
           </Button>
           </div>
         </div>
    )
}

export default BallExpress;